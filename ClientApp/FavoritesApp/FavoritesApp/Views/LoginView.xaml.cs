﻿using FavoritesApp.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace FavoritesApp.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginView : Page
    {
        public LoginView()
        {
            this.InitializeComponent();
            this.DataContext = new LoginViewModel();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            // Do something with this
            var username = this.userNameTextBox.Text;
            var password = this.passwordTextBox.Text;

            // ONLY naviate if successful
            this.Frame.Navigate(typeof(MenuItemsView));
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            // Post to the Register service
            // Do something with this
            var username = this.userNameTextBox.Text;
            var password = this.passwordTextBox.Text;

            // ONLY navigate if successful
            this.Frame.Navigate(typeof(MenuItemsView));
        }
    }
}
