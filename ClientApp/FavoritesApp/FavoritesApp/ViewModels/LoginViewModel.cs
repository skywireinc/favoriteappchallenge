﻿//using Auth0.LoginClient;
using Auth0.OidcClient;
using IdentityModel.OidcClient;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FavoritesApp.ViewModels
{
    public class LoginViewModel : BindableBase
    {
        public LoginViewModel()
        {
            _loginCommand = new DelegateCommand(async () => await Login());
            _registerCommand = new DelegateCommand(Register);
        }
        
        DelegateCommand _loginCommand;
        public DelegateCommand LoginCommand
        {
            get { return _loginCommand; }
        }

        DelegateCommand _registerCommand;
        public DelegateCommand RegisterCommand
        {
            get { return _registerCommand; }
        }

        private string _connection;
        public string Connection
        {
            get { return _connection; }
            set { this.SetProperty(ref _connection, value); }
        }

        private string _audience;

        public string Audience
        {
            get { return _audience; }
            set { this.SetProperty(ref _audience, value); }
        }

        private string _resultText;

        public string ResultText
        {
            get { return _resultText; }
            set { this.SetProperty(ref _resultText, value); }
        }

        private async Task Login()
        {
            // TODO: Setup with API 

            string domain = "skywire.auth0.com";
            string clientId = "reMVD7MigaUqtxgrirnNvH2PDYqc5LEF";

            var client = new Auth0Client(new Auth0ClientOptions
            {
                Domain = domain,
                ClientId = clientId
            });

            var extraParameters = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(this.Connection))
                extraParameters.Add("connection", this.Connection);

            if (!string.IsNullOrEmpty(this.Audience))
                extraParameters.Add("audience", this.Audience);

            // TODO: Once we know more about API endpoints plugin here

            this.DisplayResult(await client.LoginAsync(extraParameters: extraParameters));

            // if LOGIN Success
            
        }

        private void Register()
        {
            // TODO: 
        }

        private void DisplayResult(LoginResult loginResult)
        {
            // Display error
            if (loginResult.IsError)
            {
                this.ResultText = loginResult.Error;
                return;
            }

            // Display result
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Tokens");
            sb.AppendLine("------");
            sb.AppendLine($"id_token: {loginResult.IdentityToken}");
            sb.AppendLine($"access_token: {loginResult.AccessToken}");
            sb.AppendLine($"refresh_token: {loginResult.RefreshToken}");
            sb.AppendLine();

            sb.AppendLine("Claims");
            sb.AppendLine("------");
            foreach (var claim in loginResult.User.Claims)
            {
                sb.AppendLine($"{claim.Type}: {claim.Value}");
            }

            this.ResultText = sb.ToString();
        }

    }
}
