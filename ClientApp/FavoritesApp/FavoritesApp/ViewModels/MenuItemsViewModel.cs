﻿using FavoritesApp.Data;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FavoritesApp.ViewModels
{
    public class MenuItemsViewModel : BindableBase
    {
        private List<MenuItem> _tempListOfItems; 

        public MenuItemsViewModel()
        {
            #region Temp data 
            _tempListOfItems = new List<MenuItem>
            {
                new MenuItem
                {
                    Id = 1,
                    Category = "Fish",
                    Name = "Cod",
                    Price = 1.00m
                },
                new MenuItem
                {
                    Id =   2,
                    Category = "Meat",
                    Name = "Beef",
                    Price = 1.00m
                },
                new MenuItem
                {
                    Id = 3,
                    Category = "Drinks",
                    Name = "Pepsi",
                    Price = 1.00m
                },
                new MenuItem
                {
                    Id = 4,
                    Category = "Fish",
                    Name = "Bluefish",
                    Price = 1.00m
                },
                new MenuItem
                {
                    Id = 5,
                    Category = "Meat",
                    Name = "Chicken",
                    Price = 3.00m
                },
                new MenuItem
                {
                    Id = 6,
                    Category = "Candy",
                    Name = "Coke",
                    Price = 2.00m
                },
            };
            #endregion 

            _addToFavoritesCommand = new DelegateCommand(AddToFavorites);
            _removeFromFavoritesCommand = new DelegateCommand(RemoveFromFavorites);
            _menuItems = new ObservableCollection<MenuItem>();

            this.MenuItems.Clear();
            foreach (var item in _tempListOfItems)
            {
                this.MenuItems.Add(item);
            }
        }

        DelegateCommand _addToFavoritesCommand;
        public DelegateCommand AddToFavoritesCommand
        {
            get { return _addToFavoritesCommand; }
        }

        DelegateCommand _removeFromFavoritesCommand;
        public DelegateCommand RemoveFromFavoritesCommand
        {
            get { return _removeFromFavoritesCommand; }
        }

        private ObservableCollection<MenuItem> _menuItems;
        public ObservableCollection<MenuItem> MenuItems
        {
            get { return _menuItems; }
            set { this.SetProperty(ref _menuItems, value); }
        }
            
        private MenuItem _selectedMenuItem;
        public MenuItem SelectedMenuItem
        {
            get { return _selectedMenuItem; }
            set { this.SetProperty(ref _selectedMenuItem, value); }
        }

        private void AddToFavorites()
        {
            try
            {
                // Save this to the API service
                var test = this.SelectedMenuItem;
            }
            catch (Exception exception)
            {
                // LOG Error and show message
            }
        }

        private void RemoveFromFavorites()
        {
            try
            {
                // use the delete method from API 
                var test = this.SelectedMenuItem;
            }
            catch (Exception exception)
            {
                // LOG Error and show message
            }
        }
    }
}
