﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FavoritesApp.Data
{
    public class User
    {
        public List<MenuItem> MenuItems { get; set; }
    }
}
