﻿using FavoritesApp.Data;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FavoritesApp.Providers
{
    public interface IUserProvider
    {
        [Put("/api/user")]
        //[Headers("Authorization: Bearer")]
        Task UpdateUserFavorites(int id, [Body] User user);
    }
}
