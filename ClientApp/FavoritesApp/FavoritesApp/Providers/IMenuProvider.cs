﻿using FavoritesApp.Data;
using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FavoritesApp.Providers
{
    public interface IMenuProvider
    {
        [Get("/api/menu")]
        //[Headers("Authorization: Bearer")]
        Task<IEnumerable<MenuItem>> GetAllMenuItems();

        [Get("/api/favoritemenuitems")]
        //[Headers("Authorization: Bearer")]
        Task<IEnumerable<MenuItem>> GetAllFavoriteMenuItems();
    }
}
