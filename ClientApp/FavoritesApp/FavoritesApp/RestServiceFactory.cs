﻿using IdentityModel.OidcClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using System;
using System.Net.Http;

namespace FavoritesApp
{
    public class RestServiceFactory
    {
        private RefitSettings RefitSettings => new RefitSettings
        {
            JsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
            },
        };

        public TProvider Create<TProvider>(string hostUrl)
        {
            return RestService.For<TProvider>(hostUrl);
        }
    }
}
