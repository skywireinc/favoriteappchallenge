Favorite App Challenge API 
---

Favorite App Challenge is a RESTful `AspNetCore` API that will allow for a user to consume menu items to add or remove them from a users account favorites. This document covers the endpoints supported in the API.