# API Endpoints

## Get All Menu Items
```
GET /api/menu/
```
Retrieves a collection of all available menu items in the API.

An example URL: `http://localhost:9000/api/menu`

Results:

```
[
    {
        "id": 1,
        "name": "Coke",
        "category": "Beverage",
        "price": 1.19
    },
    {
        "id": 2,
        "name": "Diet Coke",
        "category": "Beverage",
        "price": 1.19
    }
]
```

## Get Favorite Menu Items
```
GET /api/favoritemenuitems/
```
Retrieves a collection of favorited menu items for the given user in the API.

An example URL: `http://localhost:9000/api/favoritemenuitems`

Results:

```
[
    {
        "id": 1,
        "name": "Coke",
        "category": "Beverage",
        "price": 1.19
    },
    {
        "id": 2,
        "name": "Diet Coke",
        "category": "Beverage",
        "price": 1.19
    }
]
```

## Favorite Menu Item
```
PUT /api/user/
```
Updates the user with the list of favorited menu items

An example URL: `http://localhost:9000/api/user/`

Request:
```
{
   "MenuItems":[
      {
         "id":1,
         "name":"Coke",
         "category":"Beverage",
         "price":1.19
      },
      {
         "id":2,
         "name":"Diet Coke",
         "category":"Beverage",
         "price":1.19
      }
   ]
}
```
