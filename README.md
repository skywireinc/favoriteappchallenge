Favorite App
===

SkyWire Cafe launched its first location last month and it was a hit!  Due to their success, they would like to provide their customers with an app to track favorite menu items.

Requirements
---

### Client App

* Must be windows desktop, web, and/or mobile app
* User can login or register
* User can logout
* User can view "My Favorites"
* User can browse through the menu and mark items as favorite


### API

* Use traditional WebApi or .Net Core to create a REST API
* API must be documented
* API must be secured
* Use menu provided in `menuitems.json`

Rules
---

1. Determine the app(s) the group wants to create
2. Determine the technologies for the app and API
3. Breakout to API and client teams
4. The API team will need to provide documentation on available endpoints
5. The client team should be able to use the documentation to complete the app

Useful Links
---

* [Auth0](https://auth0.com/)
* [JWT](https://jwt.io/)
* [Swagger](https://swagger.io/)
* [Slate](https://github.com/lord/slate)
* [Api Blueprint](https://github.com/apiaryio/api-blueprint)
* [Aglio](https://github.com/danielgtaylor/aglio)

Hint and Suggestions
---

* Use this repo to organize all solutions and projects
* It's not required to use any of the links provided in **Useful Links**
* This is the time to explore any new techs you're interested in so be creative!
* You have 7 hours to plan and develop. The last hour will be used for demo and discussion.
* HAVE FUN!

Documentation
---

Visit [FavoriteAppChallenge](http://favoriteappchallenge.readthedocs.io/en/latest/)