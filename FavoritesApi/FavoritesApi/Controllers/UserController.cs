using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FavoritesApi.ApiModels;
using FavoritesApi.Repositories;

namespace FavoritesApi.Controllers
{
    [Produces("application/json")]
    [Route("api/user")]
    public class UserController : Controller
    {
        /// <summary>
        /// Updates the user information.
        /// </summary>
        /// <param name="menuItemIds"></param>
        /// <returns></returns>
        [HttpPut]
        public IActionResult Put([FromBody]User user)
        {
            var userRepository = new UserRepository();
            userRepository.UpdateUser(user);

            return Ok();
        }

        /// <summary>
        /// Validates a provided user
        /// </summary>
        /// <param name="user">User object containing UserName and Password properties</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult ValidateUser(User user)
        {
            var repo = new UserRepository();
            if (repo.UserIsValid(user))
                return Ok(user);

            return BadRequest();
        }
    }
}