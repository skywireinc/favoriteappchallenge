using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FavoritesApi.ApiModels;
using Newtonsoft.Json;
using System.IO;
using FavoritesApi.Repositories;

namespace FavoritesApi.Controllers
{
    [Produces("application/json")]
    [Route("api/menu")]
    public class MenuController : Controller
    {
        private static readonly string MENU_ITEM_FILEPATH = Path.Combine(Directory.GetCurrentDirectory(), "menuitems.json");

        /// <summary>
        /// Gets all menu items.
        /// </summary>
        /// <returns>A list of all menu items.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<MenuItem>), 200)]
        public IActionResult Get()
        {
            var menuItemsJson = System.IO.File.ReadAllText(MENU_ITEM_FILEPATH);
            var menuItems = JsonConvert.DeserializeObject<List<MenuItem>>(menuItemsJson);

            return Ok(menuItems);
        }

        /// <summary>
        /// Gets a list of the user's favorited menu items.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of menu items.</returns>
        [HttpGet("favoritemenuitems")]
        [ProducesResponseType(typeof(List<MenuItem>), 200)]
        public IActionResult GetFavoriteMenuItems()
        {
            var userId = 0; // TODO: Retrieve the user id

            var userRepository = new UserRepository();

            var mockMenuItems = new List<MenuItem>();
            mockMenuItems.Add(new MenuItem
            {
                Id = 2,
                Name = "Diet Coke",
                Category = "Beverage",
                Price = 1.19m,
            });

            mockMenuItems.Add(new MenuItem
            {
                Id = 5,
                Name = "Powerade",
                Category = "Beverage",
                Price = 1.19m,
            });

            mockMenuItems.Add(new MenuItem
            {
                Id = 17,
                Name = "Roast Beef",
                Category = "Sandwiches",
                Price = 3.25m,
            });

            mockMenuItems.Add(new MenuItem
            {
                Id = 20,
                Name = "Chicken & Ranch",
                Category = "Sandwiches",
                Price = 3.25m,
            });

            return Ok(mockMenuItems);
        }
    }
}