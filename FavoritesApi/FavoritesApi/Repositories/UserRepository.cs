﻿using System;
using FavoritesApi.ApiModels;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace FavoritesApi.Repositories
{
    public class UserRepository
    {
        private static readonly string USERS_PATH = Path.Combine(Environment.GetEnvironmentVariable("LocalAppData"), "SkyWire/FavoriteApp/");
        private static readonly string USERS_FILE = Path.Combine(USERS_PATH, "users.json");

        public User GetUser(int id)
        {
            return this.GetUsers().FirstOrDefault(u => u.Id == id);
        }

        public bool UserIsValid(User user)
        {
            return this.GetUsers().Any(u => u.UserName == user.UserName && u.Password == user.Password);
        }

        public void CreateUser(User user)
        {
            var users = this.GetUsers().ToList();
            user.Id = users.Any() 
                ? users.Max(u => u.Id) + 1 
                : 1;

            users.Add(user);

            Directory.CreateDirectory(USERS_PATH);
            File.WriteAllText(USERS_FILE, JsonConvert.SerializeObject(users));
        }

        public void UpdateUser(User user)
        {
            var users = this.GetUsers().ToList();

            var removeUser = users.FirstOrDefault(u => u.Id == user.Id);
            if (removeUser != null)
                users.Remove(removeUser);

            users.Add(user);

            Directory.CreateDirectory(USERS_PATH);
            File.WriteAllText(USERS_FILE, JsonConvert.SerializeObject(users));
        }

        private IEnumerable<User> GetUsers()
        {
            try
            {
                var json = File.ReadAllText(USERS_FILE);

                return JsonConvert.DeserializeObject<List<User>>(json);
            }
            catch
            {
                return Enumerable.Empty<User>();
            }
        }
    }
}
