﻿using System.Collections.Generic;

namespace FavoritesApi.ApiModels
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public List<MenuItem> MenuItems { get; set; }
    }
}
