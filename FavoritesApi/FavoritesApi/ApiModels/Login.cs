﻿using System.ComponentModel.DataAnnotations;

namespace FavoritesApi.ApiModels
{
    public class Login
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
